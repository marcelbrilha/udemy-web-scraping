const fs = require("fs");
const requestPromise = require("request-promise");
const request = require("request");
const cheerio = require("cheerio");
const uuidv1 = require("uuid/v1");

// Example: Proxy
// const request = require("request-promise").defaults({
//   proxy: "http://191.37.227.128:8080"
// });

const _saveMovieImages = ({ uri, filename }) => {
  const dir = "./movieImages";

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }

  try {
    const id = uuidv1();
    const file = fs.createWriteStream(`${dir}/${filename}-${id}.jpg`);

    request({ uri }).pipe(file);
  } catch (error) {
    console.error(`Error in save image ${filename}: ${error}`);
  }
};

const getMovieDetails = url => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await requestPromise(url);
      const $ = cheerio.load(response);

      const genres = [];
      const title = $("div[class='title_wrapper'] > h1")
        .text()
        .trim();
      const rating = $("span[itemprop='ratingValue']").text();
      const poster = $("div[class='poster'] > a > img").attr("src");
      const totalRatings = $("div[class='imdbRating'] > a").text();
      const releaseDate = $("a[title='See more release dates']")
        .text()
        .trim();

      $("div[class='title_wrapper'] a[href^='/search/title?genres']").each(
        (i, element) => {
          genres.push($(element).text());
        }
      );

      // Save image - Poster
      _saveMovieImages({ filename: title, uri: poster });

      resolve({ title, rating, poster, totalRatings, releaseDate, genres });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = { getMovieDetails };
