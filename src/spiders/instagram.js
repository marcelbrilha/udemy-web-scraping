const request = require("request-promise");
const cheerio = require("cheerio");

const getInstagramDetails = async username => {
  return new Promise(async (resolve, reject) => {
    try {
      const url = `https://instagram.com/${username}`;
      const response = await request(url);
      const $ = cheerio.load(response);
      const script = $("script[type='text/javascript']")
        .eq(3)
        .html();

      const scriptRegex = /window._sharedData = (.+);/g.exec(script);
      const {
        entry_data: {
          ProfilePage: {
            [0]: {
              graphql: { user }
            }
          }
        }
      } = JSON.parse(scriptRegex[1]);

      const posts = user.edge_owner_to_timeline_media.edges.map(edge => ({
        id: edge.node.id,
        shortcode: edge.node.shortcode,
        timestamp: edge.node.taken_at_timestamp,
        likes: edge.node.edge_liked_by.count,
        coments: edge.node.edge_media_to_comment.count,
        video_views: edge.node.video_view_count,
        image_url: edge.node.display_url,
        caption: edge.node.edge_media_to_caption.edges[0]
          ? edge.node.edge_media_to_caption.edges[0].node.text
          : ""
      }));

      const instagramData = {
        followers: user.edge_followed_by,
        following: user.edge_follow.count,
        uploads: user.edge_owner_to_timeline_media.count,
        full_name: user.full_name,
        picture_url: user.profile_pic_url_hd,
        posts
      };

      resolve(instagramData);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = { getInstagramDetails };
