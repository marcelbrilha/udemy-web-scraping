const fs = require("fs");
const json2CSV = require("json2csv").Parser;
const spiderMovie = require("./spiders/movie");
const spiderInstagram = require("./spiders/instagram");

const URLS = [
  "http://www.imdb.com/title/tt0102926/?ref_=nv_sr_1",
  "http://www.imdb.com/title/tt2267998/?ref_=nv_sr_1"
];

// IMDB
(async () => {
  try {
    const promises = URLS.reduce((acc, url) => {
      acc.push(spiderMovie.getMovieDetails(url));
      return acc;
    }, []);

    const moviesData = await Promise.all(promises);

    // Save Json
    fs.writeFileSync("./data.json", JSON.stringify(moviesData), "utf-8");

    // Save CSV
    const json2CSVParser = new json2CSV();
    const csv = json2CSVParser.parse(moviesData);

    fs.writeFileSync("./data.csv", csv, "utf-8");
  } catch (error) {
    console.error(`Error in get movie details: ${error}`);
  }
})();

// Instagram
(async () => {
  try {
    const result = await spiderInstagram.getInstagramDetails("willsmith");

    // Save Json
    fs.writeFileSync("./instagram.json", JSON.stringify(result), "utf-8");
  } catch (error) {
    console.error(`Error in get instagram details: ${error}`);
  }
})();
